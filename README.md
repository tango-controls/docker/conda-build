# conda-build docker image

conda-build [docker] image to build [conda] packages.
Image inspired from <https://gitlab.esss.lu.se/ics-docker/conda-build>.

The image includes:

- Development tools
- conda-build 3

Docker pull command:

```bash
docker pull registry.gitlab.com/tango-controls/docker/conda-build
```

## How to use this image

To create a conda package, you must write a conda build recipe: <https://conda.io/docs/user-guide/tutorials/build-pkgs.html>

[docker]: https://www.docker.com
[conda]: https://conda.io/docs/index.html
