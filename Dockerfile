FROM centos:7.9.2009

LABEL MAINTAINER "TANGO Controls Team <contact@tango-controls.org>"

ENV LANG=en_US.UTF-8 LC_ALL=en_US.UTF-8 LANGUAGE=en_US.UTF-8

RUN yum update -y \
  && yum install -y epel-release \
  && yum install -y \
    which \
    bzip2 \
    wget \
    make \
    patch  \
    tar \
    dpkg \
    mesa-libGL \
    mesa-libGLU \
    xorg-x11-server-Xvfb \
  && yum clean all \
  && rm -rf /var/cache/yum \
  && mkdir /build

ENV MAMBAFORGE_INSTALLER_VERSION 23.1.0-3
ENV CONDA_VERSION 23.3.1
ENV MAMBA_VERSION 1.4.2
RUN curl -L -O "https://github.com/conda-forge/miniforge/releases/download/${MAMBAFORGE_INSTALLER_VERSION}/Mambaforge-${MAMBAFORGE_INSTALLER_VERSION}-Linux-x86_64.sh" \
  && /bin/bash "Mambaforge-${MAMBAFORGE_INSTALLER_VERSION}-Linux-x86_64.sh" -f -b -p /opt/conda \
  && rm "Mambaforge-${MAMBAFORGE_INSTALLER_VERSION}-Linux-x86_64.sh" \
  && /opt/conda/bin/conda config --system --set auto_update_conda false \
  && /opt/conda/bin/conda config --system --set show_channel_urls true \
  && /opt/conda/bin/mamba install -y conda="${CONDA_VERSION}" mamba="${MAMBA_VERSION}" \
  && /opt/conda/bin/conda clean -ay \
  && ln -s /opt/conda/etc/profile.d/conda.sh /etc/profile.d/conda.sh \
  && ln -s /opt/conda/etc/profile.d/mamba.sh /etc/profile.d/mamba.sh

ENV PATH /opt/conda/bin:$PATH

COPY entrypoint_source /entrypoint_source
COPY entrypoint /entrypoint

ENV CONDA_BUILD_VERSION=3.25.0 \
    BOA_VERSION=0.15.1

RUN mamba install -n base -y \
      conda-build="${CONDA_BUILD_VERSION}" \
      boa="${BOA_VERSION}" \
      jinja2 \
      setuptools \
      git \
      tini \
  && conda build purge-all \
  && conda clean -ay

WORKDIR /build

ENTRYPOINT [ "/opt/conda/bin/tini", "--", "/entrypoint" ]
